=====
Title
=====
Subtitle
--------

The best way to learn Angular is by example

The best way to get started with Angular is to dive in and create a web application.

Angular has a steep learning curve.

It is important the relation between node.js(npm) and angular.

https://nodejs.org/es/about/releases/

To change the nodejs version:

.. image:: nodejs.png
    :width: 700px
    :align: center
    :alt: alternate text

Really a a good why to install nodejs is following this procedure,
becouse the procedure of installing from the ubuntu repository
download the version 10! when we have already the version 16! and
the version 10 wich is a LTS will finish next month on march 2021!
the book that i am using request to use the version 12!! but this
ubuntu has the version 10! ok, entonces sigue el procedimiento este
de abajo para instalar el nvm (node version manager) y entonces con
el puedes descargar las versiones que quieras de node y pasar de una
a otra:

https://linuxhint.com/install_node-js_npm_ubuntu/

Que resumido es:

$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
$ source ~/.bashrc
$ nvm list-remote         (show the versions availables in the remote)
$ nvm install v(version number)
$ nvm install v14.9.0

or

$ nvm install lts/erbium

$ nvm list                 (show the versions installed in the host machine)

And then choice the one that you want:

$ nvm use v14.9.0


